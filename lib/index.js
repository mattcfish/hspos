var cli     = require('commander');
var path    = require('path');
var fs    = require('fs');


cli.command('start')
    .description('start the pos')
    .action(require('./pos'));

cli.command('clear')
    .description('clear data directory and set up for a new instance')
    .action(require('./init'));

    cli.command('*')
        .action(function defaultCommand(){
            console.log('Unknown command. Use agent-cli --help for a list of the available commands');
        }
    );

module.exports = cli;
