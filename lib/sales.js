var chance = new require("chance")();
var moment = require('moment');
module.exports = {
  createSales: function(){
    var sales = {};
    sales.businessDate = moment({ hour:0, minute:0 }).format('L');
    sales.tranDateTime = moment().format('MM/DD/YYYY HH:mm:ss');
    sales.amount = chance.floating({min: 0, max: 100, fixed: 2});
    sales.id = chance.integer({min:0, max: 999999999});
    sales.revCenter = chance.integer({min:1,max:5});
    sales.salesCat = chance.integer({min:1,max:5});

    sales.toString = function(){
      return this.id + "|" +
      this.businessDate + "|" +
      this.tranDateTime + "|" +
      this.amount + "|" +
      this.revCenter + "|" +
      this.salesCat;
    }

    return sales;
  }
}
