var chance = new require("chance")();

module.exports = {
  createEmployee: function(){
    var employee = {};
    employee.first = chance.first();
    employee.last = chance.last();
    employee.active = chance.bool({likelihood: 80});
    employee.id = chance.integer({min:0, max: 999999999});
    employee.dob = chance.birthday({string:true});
    employee.ssn = chance.ssn();

    employee.toString = function(){
      return this.id + "|" +
      this.last + "|" +
      this.first + "|" +
      this.active + "|" +
      this.dob + "|" +
      this.ssn;
    }
    
    return employee;
  }
}
