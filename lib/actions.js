var EventEmitter = require("events").EventEmitter;
var ee = new EventEmitter();
var fs = require('fs');
var emp = require('./employee');
var sal = require('./sales');
var async = require('async');
var chance = new require("chance")();

module.exports = {
  action : function(type){
    switch (type) {
      case 'emp':
        fs.open('./data/emp.txt', 'a+', function(err, fd) {
           if (err) {
               return console.error(err);
           }
           var employee = emp.createEmployee();
           fs.write(fd,
            employee.toString() + "\n", null, undefined, function (err, written) {
             console.log('bytes written: ' + written);
           });
          console.log(fd);
        });
        break;
      case 'sales':
        fs.open('./data/sales.txt', 'a+', function(err, fd) {
           if (err) {
               return console.error(err);
           }
           var sales = sal.createSales();
           fs.write(fd,
             sales.toString() + "\n", null, undefined, function (err, written) {
             console.log('bytes written: ' + written);
           });
          console.log(fd);
        });
        break;
      default:
    }
  },

  init : function(force){
    if(!fs.existsSync('./data/')){
      fs.mkdirSync('./data');
    }
    if(force === true){
      console.log('clearing data');
      fs.readdirSync("./data").forEach(function(fileName) {
        if((/(^|.\/)\.+[^\/\.]/g).test("./data/" + fileName)){
          return;
        }
        console.log('delete ' + fileName);
        if(fs.existsSync("./data/" + fileName)){
          fs.unlinkSync("./data/" + fileName);
          console.log('successfully deleted ' + "./data/" + fileName);
        }else{
          console.log('cannot delete ' + "./data/" + fileName);
        }
      });
    }

    var cb = function (filename, d){
      fs.open('./data/'+filename, 'a+', function(err, fd) {
         if (err) {
             return console.error(err);
         }
         async.each(d, function(item, callback){
           if (item === undefined)  return;
           fs.write(fd,
             item + "\n", null, undefined, function (err, written) {
               console.log('bytes written: ' + written);
             });
             callback();
       });
      });
    }

    fs.exists("./data/jobs.txt", function(exists){
      console.log('jobs ' + exists);
      if(exists) return;
      var data = [];
      async.each  ([1,2,3,4,5],
        function(x, callback){
         var regRate = chance.floating({min:0, max:20, fixed:2});
          data[x] = x+'|'+chance.word()+'|'+regRate+'|'+(regRate * 2);
          console.log(data[x]);
          callback();
        },
        function(err){
          console.log('done');
          cb('jobs.txt', data);
        });
    });

  fs.exists("./data/rvc.txt", function(exists){
    console.log('RVC ' + exists);
    if(exists) return;
    var data = [];
    async.each  ([1,2,3,4,5],
      function(x, callback){
       var regRate = chance.floating({min:0, max:20, fixed:2});
        data[x] = x+'|'+chance.word();
        console.log(data[x]);
        callback();
      },
      function(err){
        console.log('done');
        cb('rvc.txt', data);
      });
  });

  fs.exists("./data/cat.txt", function(exists){
    console.log('sales cats ' + exists);
    if(exists) return;
    var data = [];
    async.each  ([1,2,3,4,5],
      function(x, callback){
       var regRate = chance.floating({min:0, max:20, fixed:2});
        data[x] = x+'|'+chance.word();
        console.log(data[x]);
        callback();
      },
      function(err){
        console.log('done');
        cb('cat.txt', data);
      });
  });
}


}
